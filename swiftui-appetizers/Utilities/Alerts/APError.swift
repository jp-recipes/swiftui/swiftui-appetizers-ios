//
//  APError.swift
//  swiftui-appetizers-ios-15
//
//  Created by JP on 22-08-23.
//

import Foundation

enum APError: Error {
    case invalidURL
    case invalidResponse
    case invalidData
    case unableToComplete
}
