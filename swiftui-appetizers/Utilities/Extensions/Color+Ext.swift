//
//  Color+Ext.swift
//  swiftui-appetizers-ios-15
//
//  Created by JP on 22-08-23.
//

import SwiftUI

extension Color {
    static let brandPrimary = Color("brandPrimary")
}


extension UIColor {
    static let brandPrimary = UIColor(named: "brandPrimary")
}
