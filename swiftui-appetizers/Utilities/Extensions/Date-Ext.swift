//
//  Date.Ext.swift
//  swiftui-appetizers-ios-15
//
//  Created by JP on 23-08-23.
//

import Foundation

extension Date {
    var eighteenYearsAgo: Date {
        Calendar.current.date(byAdding: .year, value: -18, to: .now)!
    }

    var oneHundredYearsAgo: Date {
        Calendar.current.date(byAdding: .year, value: -100, to: .now)!
    }
}
