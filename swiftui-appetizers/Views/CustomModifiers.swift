//
//  CustomModifiers.swift
//  swiftui-appetizers-ios-15
//
//  Created by JP on 23-08-23.
//

import SwiftUI

struct StandarButtonStyle: ViewModifier {
    func body(content: Content) -> some View {
        content
            .buttonStyle(.bordered)
            .controlSize(.large)
            .tint(.brandPrimary)
    }
}

extension Button {
    func standarButtonStyle() -> some View {
        modifier(StandarButtonStyle())
    }
}

extension Image {
    func standarImageStyle() -> some View {
        self.resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width: 120, height: 90)
            .cornerRadius(8)
    }
}
