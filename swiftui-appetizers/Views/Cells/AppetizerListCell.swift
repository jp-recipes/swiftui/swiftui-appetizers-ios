//
//  AppetizerListCell.swift
//  swiftui-appetizers-ios-15
//
//  Created by JP on 22-08-23.
//

import SwiftUI

struct AppetizerListCell: View {
    let appetizer: Appetizer

    var body: some View {
        HStack {
            // Before iOS 15: AsyncImage
            //AppetizerRemoteImage(urlString: appetizer.imageURL)
            //    .aspectRatio(contentMode: .fit)
            //    .aspectRatio(contentMode: .fit)
            //    .cornerRadius(8)

            // Doesn't have automatic cache
            AsyncImage(url: URL(string: appetizer.imageURL)) { image in
                image.standarImageStyle()
            } placeholder: {
                Image("food-placeholder").standarImageStyle()
            }

            VStack(alignment: .leading, spacing: 5) {
                Text(appetizer.name)
                    .font(.title2)
                    .fontWeight(.medium)

                Text("$\(appetizer.price, specifier: "%.2f")")
                    .foregroundColor(.secondary)
                    .fontWeight(.semibold)
            }
            .padding(.leading)
        }
    }
}

struct AppetizerListCell_Previews: PreviewProvider {
    static var previews: some View {
        AppetizerListCell(appetizer: MockData.sampleAppetizer)
    }
}
