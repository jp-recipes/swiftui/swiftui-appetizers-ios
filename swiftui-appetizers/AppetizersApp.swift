//
//  swiftui_appetizers_ios_15App.swift
//  swiftui-appetizers-ios-15
//
//  Created by JP on 22-08-23.
//

import SwiftUI

@main
struct AppetizersApp: App {
    var order = Order()

    var body: some Scene {
        WindowGroup {
            AppetizerTabView().environmentObject(order)
        }
    }
}
