//
//  User.swift
//  swiftui-appetizers-ios-15
//
//  Created by JP on 22-08-23.
//

import Foundation

struct User: Codable {
    var firstName = ""
    var lastName = ""
    var email = ""
    var birthday = Date()
    var extraNapkins = false
    var frequentRefills = false
}
