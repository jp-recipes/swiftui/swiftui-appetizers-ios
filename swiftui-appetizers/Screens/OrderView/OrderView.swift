//
//  OrderView.swift
//  swiftui-appetizers-ios-15
//
//  Created by JP on 22-08-23.
//

import SwiftUI

struct OrderView: View {
    @EnvironmentObject var order: Order
    
    var body: some View {
        NavigationView {
            ZStack {
                VStack {
                    List {
                        ForEach(order.items) { appetizer in
                            AppetizerListCell(appetizer: appetizer)
                        }
                        .onDelete(perform: order.deleteItems(at:))
                    }
                    .listStyle(PlainListStyle())
                    
                    Button {
                        
                    } label: {
                        // Before iOS 15 Button Styles
                        //APButton(title: "$\(order.totalPrice, specifier: "%.2f") - Place Order")
                        Text("$\(order.totalPrice, specifier: "%.2f") - Place Order")
                    }
                    // iOS 15
                    //.modifier(StandarButtonStyle())
                    .standarButtonStyle()
                    .padding(.bottom, 25)
                }
                
                if order.items.isEmpty {
                    EmptyState(
                        imageName: "empty-orders",
                        message: "You have no items in your order. Please add an appetizer!."
                    )
                }
            }
            .navigationTitle("Orders")
        }
    }
}

struct OrderView_Previews: PreviewProvider {
    static var previews: some View {
        let order = Order()
        order.items = MockData.orders
        return OrderView().environmentObject(order)
    }
}
