//
//  ContentView.swift
//  swiftui-appetizers-ios-15
//
//  Created by JP on 22-08-23.
//

import SwiftUI

struct AppetizerTabView: View {
    @EnvironmentObject var order: Order
    
    var body: some View {
        TabView {
            AppetizerListView()
                .tabItem { Label("Home", systemImage: "house") }
            
            AccountView()
                .tabItem { Label("Account", systemImage: "person") }
            
            OrderView()
                .tabItem { Label("Order", systemImage: "bag") }
                .badge(order.items.count)
        }
        // Before iOS 15
        //.accentColor(.brandPrimary)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let order = Order()
        order.items = MockData.orders
        return AppetizerTabView().environmentObject(order)
    }
}
