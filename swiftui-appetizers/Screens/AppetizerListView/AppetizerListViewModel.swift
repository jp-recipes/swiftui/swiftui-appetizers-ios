//
//  AppetizerListViewModel.swift
//  swiftui-appetizers-ios-15
//
//  Created by JP on 22-08-23.
//

import Foundation

@MainActor
final class AppetizerListViewModel: ObservableObject {
    @Published var appetizers: [Appetizer] = []
    @Published var alertItem: AlertItem?
    @Published var isLoading = false
    @Published var isShowingDetail = false
    @Published var selectedAppetizer: Appetizer?

    func getAppetizers() {
        isLoading = true

        Task { // Async Context
            do {
                appetizers = try await NetworkManager.shared.getAppetizers()
            } catch {
                switch error as? APError {
                case .invalidResponse:
                    alertItem = AlertContext.invalidResponse
                case .invalidURL:
                    alertItem = AlertContext.invalidURL
                case .invalidData:
                    alertItem = AlertContext.invalidData
                case .unableToComplete:
                    alertItem = AlertContext.unableToComplete
                case .none:
                    alertItem = AlertContext.invalidResponse
                }
            }

            isLoading = false
        }
    }

    /*func getAppetizers() {
        isLoading = true
        NetworkManager.shared.getAppetizers { [self] result in
            DispatchQueue.main.async {
                self.isLoading = false

                switch result {
                case .success(let appetizers):
                    self.appetizers = appetizers
                case .failure(let error):
                    switch error {
                    case .invalidResponse:
                        self.alertItem = AlertContext.invalidResponse
                    case .invalidURL:
                        self.alertItem = AlertContext.invalidURL
                    case .invalidData:
                        self.alertItem = AlertContext.invalidData
                    case .unableToComplete:
                        self.alertItem = AlertContext.unableToComplete
                    }
                }
            }
        }
    }*/
}
